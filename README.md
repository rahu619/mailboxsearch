# MailboxSearch

MailboxSearch prototype


**Phase One (Searching email folders):** 
Implement an API that could possibly retrieve the searched emails from the configured users inbox content.
There will be multiple users and the API will specify where to filter message from.

> If there are multiple matches found for the search, returning only the latest eml (atleast for now).
> If file attachments has to be downloaded along with the eml, then zip might be the work around.


**Phase Two (Receiving updates):**
Relying on Inbox Count Change Event.
_Perform search everytime a mail arrives in the folder? _
In that case, 
The entity user will have a **Search Collection** associated with it.
This Search collection can also be modified at any given point of time.
    
> HttpGet - If we aren't persisting the search term 
    `/api/v1/{Client_ID}/search?FromEmailContains=&SubjectContains=Testing&DeliveredAfter=29-03-2021`

> HttpPost - Storing Search term for later use 
   ` /api/v1/{Client_ID}/search/`
    
    FromEmailContains,
    SubjectContains,
    DeliveredAfter,
    IsPersistent (If yes, then perform search against the "search term" for the configured particular user)


**Phase Three (Background Service):**
A Hosted Service to setup all the Mail service and the email clients will be maintained here. 
This can be migrated to a normal service application at a later stage.
_Memory usage and thorough testing needs to be done!_

**Phase Four (Onedrive implementation):**
Onedrive API implementation.


